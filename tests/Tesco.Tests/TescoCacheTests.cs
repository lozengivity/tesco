﻿using NUnit.Framework;
using TescoCache.Client;

namespace Tesco.Tests
{
    [TestFixture]
    public class TescoCacheTests
    {

        [Test]
        public void RetrieveSingleItem()
        {
            using(var server = new TestServer(1))
            {
                var c = new CacheClient(Helpers.IPAddress, 8001);
                c.Put("A", "1").Wait();
                var result = c.Get("A").Result;
                Assert.AreEqual("1\n", result);
            }
        }

        [Test]
        public void CanUpdateExistingKey()
        {
            using (var server = new TestServer(1))
            {
                var c = new CacheClient(Helpers.IPAddress, 8001);
                c.Put("A", "1").Wait();
                c.Put("A", "2").Wait();
                var result = c.Get("A").Result;
                Assert.AreEqual("2\n", result);
            }
        }

        [Test]
        public void CanAddItemsExceedingCacheSize()
        {
            using (var server = new TestServer(1))
            {
                var c = new CacheClient(Helpers.IPAddress, 8001);
                c.Put("A", "1").Wait();
                c.Put("B", "2").Wait();
                c.Put("C", "3").Wait();
                var result = c.Get("C").Result;
                Assert.AreEqual("3\n", result);
            }
        }

        [Test]
        public void CannotRetrieveItemsKickedOutOfCache()
        {
            using (var server = new TestServer(1))
            {
                var c = new CacheClient(Helpers.IPAddress, 8001);
                c.Put("A", "1").Wait();
                c.Put("B", "2").Wait();
                c.Put("C", "3").Wait();
                var result = c.Get("A").Result;
                Assert.AreEqual("\n", result);
            }
        }
    }
}
