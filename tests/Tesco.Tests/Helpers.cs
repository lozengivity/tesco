﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TescoCache;

namespace Tesco.Tests
{
    public static class Helpers
    {
        public static string IPAddress
        {
            get
            {
                string hostName = Dns.GetHostName();
                IPHostEntry ipHostInfo = Dns.GetHostEntry(hostName);
                IPAddress ipAddress = null;
                for (int i = 0; i < ipHostInfo.AddressList.Length; ++i)
                {
                    if (ipHostInfo.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = ipHostInfo.AddressList[i];
                        break;
                    }
                }
                return ipAddress.ToString();
            }
        }
    }

    public class TestServer : IDisposable
    {
        CancellationTokenSource _cts;
        Task _serverTask;

        public TestServer(int size)
        {
            _cts = new CancellationTokenSource();
            _serverTask = ServerFactory.Run(size, 8001, _cts.Token);
        }

        public void Dispose()
        {
            _cts.Cancel();
        }
    }
}
