﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TescoCache.Client
{
    public class CacheClient
    {
        private string _host;
        private int _port;

        public CacheClient(string host, int port)
        {
            _host = host;
            _port = port;
        }

        public async Task<string> Get(string key)
        {
            var message = string.Format("GET\n{0}", key);
            return await Transmit(message); 
        }

        public async Task<string> Put(string key, string value)
        {
            var message = string.Format("PUT\n{0}\n{1}", key, value);
            return await Transmit(message);
        }

        private async Task<string> Transmit(string message)
        {
            using (var client = new TcpClient())
            {
                client.Connect(_host, _port);
                var stream = client.GetStream();
                ASCIIEncoding asen = new ASCIIEncoding();
                byte[] ba = asen.GetBytes(message);

                await stream.WriteAsync(ba, 0, ba.Length);
                await stream.FlushAsync();

                byte[] bb = new byte[4096];
                int k = await stream.ReadAsync(bb, 0, 4096);

                var sb = new StringBuilder();
                for (int i = 0; i < k; i++)
                    sb.Append(Convert.ToChar(bb[i]));

                return sb.ToString();
            }            
        }
    }
}
