﻿using System;
using System.Threading.Tasks;

namespace TescoCache.ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Test().Wait();
            Console.WriteLine("Finished...");
            Console.ReadLine();
        }

        public static async Task Insert(Client.CacheClient client, string key, string value)
        {
            var response = await client.Put(key, value);
            Console.WriteLine("Inserted {0} with value {1} in to cache", key, value);
        }

        public static async Task Retrieve(Client.CacheClient client, string key)
        {
            var response = await client.Get(key);
            Console.WriteLine("Retrieved {0} with value {1} in to cache", key, response);
        }

        public static async Task Test()
        {
            var c = new Client.CacheClient("10.21.6.117", 8001);

            await Insert(c, "a", "1");
            await Insert(c, "b", "2");
            await Insert(c, "c", "3");
            await Insert(c, "d", "4");
            await Insert(c, "e", "5");
            await Insert(c, "f", "6");
            await Retrieve(c, "a");
            await Retrieve(c, "b");

        

        }
    }


}
