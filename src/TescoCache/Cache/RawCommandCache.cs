﻿namespace TescoCache.Cache
{
    class RawCommandCache
    {
        private ICache _cache;

        public RawCommandCache(ICache cache)
        {
            _cache = cache;
        }

        public string GetResponse(string s)
        {
            var lines = s.Split('\n');
            switch (lines[0])
            {
                case "PUT":
                    if (lines.Length != 3)
                        throw new System.ArgumentException("PUT command does not follow correct protocol");
                    var putKey = lines[1];
                    var putVal = lines[2];
                    var putResult = _cache.Put(putKey, putVal);
                    return putResult.Response;

                case "GET":
                    if (lines.Length != 2)
                        throw new System.ArgumentException("GET command does not follow correct protocol");
                    var getKey = lines[1];
                    var getResult = _cache.Get(getKey);
                    return getResult.Response;

                default:
                    throw new System.InvalidOperationException("Unrecognised command");
            }
        }
    }
}
