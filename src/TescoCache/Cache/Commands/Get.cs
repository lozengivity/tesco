﻿namespace TescoCache.Cache.Commands
{
    public interface ICommand { }

    public class GetArgs
    {
        public string Key { get; set; }
    }

    public class GetResult
    {
        public string Response { get; set; }
    }
}
