﻿namespace TescoCache.Cache.Commands
{
    public class PutArgs
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class PutResult
    {
        public string Response { get; set; }
    }
}
