﻿using System;
using System.Collections.Generic;
using TescoCache.Cache.Commands;
using System.Linq;

namespace TescoCache.Cache
{
    class SimpleCache : ICache
    {
        private int _size;
        private Dictionary<string, CacheEntry> _cache;
        private readonly object _padlock;

        public SimpleCache(int size)
        {
            _size = size;
            _cache = new Dictionary<string, CacheEntry>();
            _padlock = new object();
        }

        public PutResult Put(string key, string value)
        {
            lock (_padlock)
            {
                try
                {
                    
                    if (_cache.Count >= _size)
                    {
                        var keyToRemove = _cache.OrderBy(kvp => kvp.Value.TimeEntered).First().Key;
                        _cache.Remove(keyToRemove);
                    }

                    if (!_cache.ContainsKey(key))
                        _cache.Add(key, new CacheEntry(value));
                    else _cache[key] = new CacheEntry(value);
                    return new PutResult { Response = "OK\n" };
                }

                catch (Exception e)
                {
                    return new PutResult { Response = "FAIL\n" };
                }
            }

        }

        public GetResult Get(string key)
        {
            lock (_padlock)
            {
                if (_cache.ContainsKey(key)) return new GetResult { Response = string.Format("{0}\n", _cache[key].Value) };
                else return new GetResult { Response = "\n" };
            }
        }

        private class CacheEntry
        {
            public CacheEntry(string value)
            {
                Value = value;
                TimeEntered = System.DateTime.Now;
            }

            public DateTime TimeEntered { get; set; }
            public string Value { get; set; }
        }
    }
}
