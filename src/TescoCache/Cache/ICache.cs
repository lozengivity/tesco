﻿using TescoCache.Cache.Commands;

namespace TescoCache.Cache
{
    public interface ICache
    {
        PutResult Put(string key, string value);
        GetResult Get(string key);
    }
}
