﻿using System;
using System.Threading;
using System.Threading.Tasks;
using TescoCache.Cache;
using TescoCache.Server;

namespace TescoCache
{
    public static class ServerFactory
    {
        public static async Task Run(int size, int port, CancellationToken cancellation)
        {
            var innerCache = new SimpleCache(size);
            var rawCache = new RawCommandCache(innerCache);
            var server = new TescoCacheServer(new ConsoleLogger(), rawCache);
            server.Run(port);
            await Task.Delay(TimeSpan.MaxValue, cancellation);
            server.Dispose();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var cts = new CancellationTokenSource();
                var server = ServerFactory.Run(5, 8001, cts.Token);
                Console.ReadLine();
                cts.Cancel();

            }
            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
            }
        }
    }
}
