﻿using System;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using TescoCache.Cache;

namespace TescoCache.Server
{
    public abstract class TcpServerBase : IDisposable
    {
        private readonly ILogger _logger;
        private readonly IPAddress _ipAddress;
        private bool _isServerRunning;
        private TcpListener _listener;

        protected TcpServerBase(ILogger logger)
        {
            _logger = logger;
            _isServerRunning = true;
            string hostName = Dns.GetHostName();
            var ipHostInfo = Dns.GetHostEntry(hostName);
            _ipAddress = null;
            for (int i = 0; i < ipHostInfo.AddressList.Length; ++i)
            {
                if (ipHostInfo.AddressList[i].AddressFamily == AddressFamily.InterNetwork)
                {
                    _ipAddress = ipHostInfo.AddressList[i];
                    break;
                }
            }
            if (_ipAddress == null) throw new Exception("No IPv4 address for server");
        }

        public async void Run(int port = 0, int bufferSize = 4096)
        {
            _listener = new TcpListener(_ipAddress, port);
            _listener.Start();
            _logger.Info(string.Format("Tcp Server Listening on {0}:{1}", IPAddress, Port));

            while (_isServerRunning)
            {
                try
                {
                    var tcpClient = await _listener.AcceptTcpClientAsync();

                    string clientEndPoint = tcpClient.Client.RemoteEndPoint.ToString();
                    _logger.Info(string.Format("Received connection request from {0}", clientEndPoint));

                    var stream = tcpClient.GetStream();
                    var buffer = new byte[bufferSize];
                    var amountRead = await stream.ReadAsync(buffer, 0, bufferSize);
                    var memoryStream = new MemoryStream(buffer, 0, amountRead);

                    var result = await HandleMessage(memoryStream);

                    Byte[] sendBytes = null;
                    try
                    {
    
                        using (BinaryReader br = new BinaryReader(result))
                        {
                            sendBytes = br.ReadBytes((int)result.Length);
                        }
                        stream.Write(sendBytes, 0, sendBytes.Length);
                        stream.Flush();
                    }
                    catch (SocketException e)
                    {
                        throw;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        protected abstract Task<Stream> HandleMessage(Stream stream);

        public int Port
        {
            get
            {
                if (_listener == null) throw new Exception("TcpListener has not been initialised");
                var ipEndPoint = (IPEndPoint)_listener.LocalEndpoint;
                return ipEndPoint.Port;
            }
        }

        public string IPAddress
        {
            get
            {
                return _ipAddress.ToString();
            }
        }

        public void Dispose()
        {
            _listener.Stop();
            _isServerRunning = false;
        }
    }

    class TescoCacheServer : TcpServerBase
    {
        RawCommandCache _cache;

        public TescoCacheServer(ILogger logger, RawCommandCache cache)
            : base(logger)
        {
            _cache = cache;
        }

        protected override async Task<Stream> HandleMessage(Stream stream)
        {
            StreamReader reader = new StreamReader(stream);
            string message = await reader.ReadToEndAsync();
            var response = _cache.GetResponse(message);
            byte[] byteArray = Encoding.ASCII.GetBytes(response);
            MemoryStream stream2 = new MemoryStream(byteArray);
            return stream2;
        }
    }
}

