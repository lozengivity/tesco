Tesco Key-Value Cache
=====================

Note: I wrote this readme after the 2 hr cut-off time.

This repository contains

* Simple Key-Value Server (`/src/TescoCache`)
* Client Library (`/src/TescoCache.Client`)

There are (scant!) tests for this server in `/tests/Tesco.Tests`.

The tests are written using `NUnit`, and (I hope) you can run them by running the following command from the repository root:

    build.cmd Watch

This command will run the tests, and should re-compile and re-run the tests on every file change to `/src` or `/tests` directories.

