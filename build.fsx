#r "packages/Fake/tools/FakeLib.dll"

open Fake

let buildRoot = __SOURCE_DIRECTORY__ @@ "build"

let setBuildParams dir defaults = 
  { defaults with
      Verbosity = Some Quiet
      Targets = ["Build"]
      Properties =
          [
              "Optimize", "True"
              "DebugSymbols", "True"
              "Configuration", "Debug"
              "OutputPath", buildRoot @@  dir
          ]
  }

let make () = 
  !! "./**/*.csproj"
  |> Seq.iter(fun a -> 
    let directory = a |> directory |> directoryInfo
    let directoryName = directory.Name
    trace (sprintf "Building %s" directoryName)
    build (setBuildParams directoryName) a
    ())

let runTests() = 
  !! (buildRoot + "/**/*Tests.dll")
  |> Seq.iter(fun a -> 
    printfn "Running tests in %s" a
    let x = 
      ExecProcess (fun psi -> 
        psi.FileName <- "./packages/NUnit.ConsoleRunner/tools/nunit3-console.exe"
        psi.Arguments <- a) (System.TimeSpan.FromMinutes(5.0))
    ())

Target "Watch" (fun _ -> 
  use srcWatcher = 
    !! "src/**/*.*"
    ++ "tests/**/*.*"
    |> WatchChanges(fun e -> 
        printfn "Changed files"
        for f in e do printfn " - %s" f.Name
        try
          make()
          runTests()
        with e -> 
          e |> sprintf  "Something bad happened: %A" |>  traceError
      )

  make()
  runTests()
  System.Console.ReadLine() |> ignore
      
  ())

Target "Build" (fun _ -> make())
Target "Test" (fun _ -> runTests())

RunTargetOrDefault "Build"